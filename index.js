console.log("Hello World")

let firstName = "Collene", lastName = "De Silva", age = "24", hobbies = ["Gaming", "Coding", "Streaming"]

let address = {
	houseNumber : "230",
		street : "Azucena",
		city : "Malolos",
		state : "Bulacan",	
}

console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);
console.log("Hobbies:");
console.log(hobbies);
console.log("Work Address:");
console.log(address)

function printUserInfo(firstName, lastName, age, hobbies, address) {
	console.log(firstName + " " + lastName + " is " + age + " years of age.");
	console.log('This was printed inside of the function');
	console.log(hobbies);
	console.log('This was printed inside of the function');
	console.log(address);
}

printUserInfo(firstName, lastName, age, hobbies, address);

//Mini Activity: 

let myFavoriteFood = "Buffalo Wings";
let sum = 150 + 9;
let product = 100 * 90;
let isUserActive = true;
let myFavoriteRestaurants = ["Blakes", "Frankies", "Ippudo", "Jollibee", "Army Navy"];
let myFavoriteArtist = {
	firstName : "Jisoo",
	lastName: 'Kim',
	stageName : "BLACKPINK Jisoo",
	birthDay : "01/03/1995",
	age : 27,
	bestAlbum : "The Album",
	bestSong : "Lovesick Girls"
	isActive : true
}

console.log(myFavoriteFood)
console.log(sum)
console.log(product)
console.log(isUserActive)
console.log(myFavoriteRestaurants)
console.log(myFavoriteArtist)

function getQuotient(firstNumber, secondNumber) {
	return firstNumber/secondNumber
}

console.log(getQuotient(100,10))